update products
set properties = JSON_OBJECT(
	'weight', 10, 
    'dimensions', JSON_ARRAY(1, 2, 3),
    'manufacturer', JSON_OBJECT(
		'name', 'sony'
    )
)
where product_id = 1;

update products
set properties = JSON_SET(
	properties,
	'$.weight', 20,
    '$.age', 10
)
where product_id = 1;

update products
set properties = JSON_REMOVE(
	properties,
    '$.age'
)
where product_id = 1;


select 
	product_id,
    properties
from products
where product_id = 1;