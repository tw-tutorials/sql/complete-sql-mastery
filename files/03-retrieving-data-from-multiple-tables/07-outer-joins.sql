select 
	c.customer_id,
    c.first_name,
    o.order_id
from customers c
left join orders o # return all rows from left table 'customers'
	on c.customer_id = o.customer_id
order by c.customer_id;

# Exercise
select
	p.product_id,
    p.name,
    oi.quantity
from products p
left join order_items oi
	on p.product_id = oi.product_id
order by p.product_id;