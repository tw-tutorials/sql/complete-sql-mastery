select 
	c.first_name as customer
from customers c, orders o
order by c.first_name;

# Exercise
select *
from shippers s
cross join products p;

select *
from shippers s, products p;