#select order_id, o
.customer_id, first_name, last_name
from orders o -- o: alias for orders
join customers c -- c: alias for customers
	on o.customer_id = c.customer_id;

-- Exercise
select order_id, p.product_id, name, quantity, oi.unit_price
from order_items oi
    join products p on oi.product_id = p.product_id;