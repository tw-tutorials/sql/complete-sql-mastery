delimiter $$
create procedure get_clients()
begin
	select * from clients;
end$$

delimiter ;


call get_clients();


# Exercise
delimiter $$
create procedure get_invoices_with_balance()
begin
	select * from invoices_with_balance where balance > 0;
end$$
delimiter ;

call get_invoices_with_balance;