select now(), curdate(), curtime();
select year(now());
select month(now());
select day(now());
select hour(now());
select minute(now());
select second(now());
select monthname(now());
select dayname(now());
select extract(year from now());

# Exercise
select *
from orders
where year(order_date) >= year(now());