select length('sky');
select upper('sky');
select lower('Sky');
select ltrim('    Sky');
select rtrim('Sky    ');
select trim('  Sky  ');
select left('Kindergarten', 4);
select right('Kindergarten', 6);
select substring('Kindergarten', 3, 5);
select locate('Q', 'Kindergarten');
select replace('Kindergarten', 'garten', 'garden');
select concat('first', 'last');

use sql_store;
select concat(first_name, ' ', last_name) as full_name
from customers