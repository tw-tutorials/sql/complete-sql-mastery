use sql_store;

select 
	order_id,
    ifnull(shipper_id, 'Not assigned') as shipper
from orders;

select 
	order_id,
    ifnull(shipper_id, '...'),
    coalesce(shipper_id, comments, 'Not assigned') as shipper
from orders;

# Exercise
select
	concat(first_name, ' ', last_name) as customer,
    ifnull(phone, 'Unknown') as phone
from customers;