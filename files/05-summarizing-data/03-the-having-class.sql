select
	client_id,
    sum(invoice_total) as total_sales,
    count(*) as number_of_invoices
from invoices
group by client_id
having # Filter data after row grouping (only selected columns will work)
	total_sales > 500 and
    number_of_invoices > 5;
    
    
# Exercise
use sql_store;
select
	c.customer_id,
    c.first_name,
    c.last_name,
    sum(oi.quantity * oi.unit_price) as total_sales
from customers c
join orders o using (customer_id)
join order_items oi using (order_id)
where state = 'VA'
group by
	c.customer_id,
    c.first_name,
    c.last_name
having total_sales > 100






