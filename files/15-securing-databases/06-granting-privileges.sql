-- 1: web/desktop application
create user moon_app identified by '1234';

grant select, insert, update, delete, execute
on sql_store.*
to moon_app;

-- 2: admin
grant all
on *.*
to john;