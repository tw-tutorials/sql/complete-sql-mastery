grant create view
on sql_store.*
to moon_app;

revoke create view
on sql_store.*
from moon_app;