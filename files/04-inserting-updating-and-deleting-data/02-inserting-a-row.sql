insert into customers
values (
	default, 
    'John', 
    'Smith', 
    '1990-01-01',
    null,
    'address',
    'city',
    'CA',
    default
);

insert into customers (
	first_name,
    last_name,
    birth_date,
    address,
    city,
    state
)
values (
    'John', 
    'Smith', 
    '1990-01-01',
    'address',
    'city',
    'CA'
);