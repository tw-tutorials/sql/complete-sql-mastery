insert into shippers (name)
values 
	('Shipper1'),
	('Shipper2'),
    ('Shipper3');
    
# Exercise
insert into products (name, quantity_in_stock, unit_price)
values
	('Product 1', 10, 10.0),
    ('Product 2', 20, 20.0),
    ('Product 3', 30, 30.0);