SELECT *
FROM customers
-- WHERE last_name LIKE '%field%'
-- WHERE last_name REGEXP 'field$|mac|rose';
-- WHERE last_name REGEXP '[gim]e';
where last_name regexp '[a-h]e';
-- ^ beginning
-- $ end
-- | logical or
-- [abcde]
-- [a-f]

-- Exercise
select * from customers where first_name regexp '^elka$|^ambur$';
select * from customers where last_name regexp 'ey$|on$';
select * from customers where last_name regexp '^my|se';
select * from customers where last_name regexp 'b[ru]';