select *
from customers
order by state desc, first_name;

select first_name, last_name, 10 as points
from customers
order by state desc, first_name;

select first_name, last_name, 10 as points
from customers
order by points, first_name;

-- Exercise
select *, quantity * unit_price as total_price
from order_items
where order_id = 2
order by total_price desc;