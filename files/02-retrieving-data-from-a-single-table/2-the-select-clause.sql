SELECT 
	first_name, 
    last_name, 
    points, 
    (points + 10) * 100 AS 'discount factor'
FROM customers;

SELECT distinct state 
FROM customers;

-- Exercise
SELECT
	name,
    unit_price AS 'unit price',
    (unit_price * 1.1) AS 'new price'
FROM products;