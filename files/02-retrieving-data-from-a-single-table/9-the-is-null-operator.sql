select * from customers where phone is null;
select * from customers where phone is not null;

-- Exercise: get the orders that are not shipped yet
select * from orders where shipped_date is null OR shipper_id is null;