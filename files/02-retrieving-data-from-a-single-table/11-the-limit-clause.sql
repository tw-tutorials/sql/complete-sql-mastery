select *
from customers
limit 6, 3; -- offset: 6, pick: 3
-- page 1: 1 - 3
-- page 2: 4 - 6
-- page 3: 7 - 9

-- Exercise
select *
from customers
order by points desc
limit 3;