show variables like 'event%';
set global event_scheduler = ON;


delimiter $$
create event yearly_delete_stale_audit_rows
on schedule
	-- at '2019-05-01' -- execute once
    every 1 year 
    starts '2019-01-01'
    ends '2029-01-01'
do begin
	delete from payments_audit
    where action_date < now() - interval 1 year;
end$$
delimiter ;