# Find products that are more expensive than lettuce (id = 3)
select *
from products
where unit_price > (
	select unit_price
    from products
    where product_id = 3
);

# Exercise
# find employees whose earn more than average
use sql_hr;
select *
from employees
where salary > (
	select 
		avg(salary)
	from employees
);