# Find clients without invoices
select *
from clients
where client_id not in (
	select distinct client_id 
    from invoices
);

select *
from clients
left join invoices using (client_id)
where invoice_id is null;


# Exercise
# Find customers who have ordered lettuce (id = 3)
#	select customer_id, first_name, last_name
use sql_store;
select customer_id, first_name, last_name
from customers
where customer_id in (
	select distinct customer_id 
    from orders 
    where order_id in (
		select distinct order_id 
        from order_items 
        where product_id = 3
    )
);

# Mosh's approach #1
select customer_id, first_name, last_name
from customers
where customer_id in (
	select o.customer_id
    from order_items oi
    join orders o using (order_id)
    where product_id = 3
);
# Mosh's approach #2
select distinct customer_id, first_name, last_name
from customers c
join orders o using (customer_id)
join order_items oi using (order_id)
where oi.product_id = 3;