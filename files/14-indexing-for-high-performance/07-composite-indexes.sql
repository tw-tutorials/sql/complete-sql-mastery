use sql_store;

show indexes in customers;

explain select customer_id
from customers
where state = 'CA' and points > 1000;

create index idx_state_points on customers (state, points);

drop index idx_state on customers;