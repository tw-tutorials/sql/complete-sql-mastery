explain select customer_id
from customers
where state = 'CA';

select count(*) from customers;

create index idx_state
on customers (state);


# Exercise
explain select customer_id from customers where points > 1000;
create index idx_points on customers (points);