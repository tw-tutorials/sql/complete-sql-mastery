use sql_blog;

select * 
from posts 
where title like '%react redux%'
	or body like '%react redux%';
    
    
create fulltext index idx_body on posts (title, body);

select 
	*, 
    match(title, body) against('react redux') as relevance_score
from posts 
where match(title, body) against('react -redux +"handling a form"' in boolean mode);